FROM node:carbon-alpine

ENV APP /opt/app
WORKDIR $APP

COPY package.json package-lock.json $APP/
RUN npm install

COPY . $APP

EXPOSE 9080
CMD ["npm", "start"]
