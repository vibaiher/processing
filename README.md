Processing
==========

This is just a sample of how to use Processing.js with `node`, `npm` and `docker`.

### Installation / build

- Requirements `docker`

### Installation / build

- Run `docker build -t processing .`

### Start local server

- Run `docker run --rm -p 9080:9080 processing`
- Visit http://localhost:9080

\* 9080 port is configured in package.json
